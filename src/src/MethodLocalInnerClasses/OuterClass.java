package MethodLocalInnerClasses;

/**
 * Created by Erick on 24/07/2017.
 */
public class OuterClass {
    private int i = 9;

    //Se crea la instacia de una inner class y se llama a la función de la inner classç
    public void innerMethod() {
        //se declara el metodo interno de la inner class
        class InnerClass {
            public void getValue() {
                //Se accesa a la variable privada desde la outer class
                System.out.println("value of i -" + i);
            }
        }
        //Se instancia la inner class
        InnerClass i1 = new InnerClass();
        i1.getValue();
    }
}
