package InnerClasses;

/**
 * Created by Erick on 24/07/2017.
 */
public class DataStructure {

    //Creación de un array
    private final static int SIZE = 15;
    private int[] arrayOfInts = new int[SIZE];

    /**
     * Constructor: acceso sin problema a las variables.
     */

    public DataStructure() {
        //Se llena el array con numeros en forma ascendente
        for (int i = 0; i < SIZE; i++) {
            arrayOfInts[i] = i;
        }
    }

    /**
     * PrintEven, creamos un InnerEventIterator (clase interna, que tiene íntima relación
     * con nuestra clase) y vamos recorriendo e imprimiendo los valores pares del array.
     */

    public void printEven() {
        //Se imprime los calores de los indices pares del array
        InnerEvenIterator iterator = this.new InnerEvenIterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.getNext() + " ");
        }
    }

    /**
     * Clase interna.
     * InnerEventIterator: tiene un atributo next y después dos métodos internos, que pueden acceder
     * a arrayOfInts (atributo interno de DataStructure.
     */

    private class InnerEvenIterator {
        //Contador, que empieza en la posición 0
        private int next = 0;
        public boolean hasNext() {
            //Comprueba si el elemento actual es el último dentro del array
            return (next <= SIZE - 1);
        }

        public int getNext() {
            //Registra un valor dentro del índice par del array.
            int retValue = arrayOfInts[next];
            //Obtiene el siguiente elemento y lo devuelve
            next += 2;
            return retValue;
        }
    }

    /**
     * Método main de prueba
     * @param s
     */
    public static void main(String s[]) {
        //Cremos un objeto y llamamos al método printEvent, que hemos visto que creará
        // una clase inner interna para gestionar el método
        DataStructure ds = new DataStructure();
        ds.printEven();
    }
}
