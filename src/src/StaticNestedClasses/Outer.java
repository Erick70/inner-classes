package StaticNestedClasses;

/**
 * Created by Erick on 24/07/2017.
 */
public class Outer {
    //Se crea la clase Inner
    static class Inner{
        //Se crea in método dentro de Inner
        public static void testMethod(){
            System.out.println("Test");
        }

        //Se crea un segundo método dentro de Inner
        public void testMethod2(){
            System.out.println("Test2");
        }
    }
    public static void main(String[] args) {
        //Se llama al primer método de la clase Inner
        Outer.Inner.testMethod();

        //Se instancia la clase Inner y se llama al segundo método de la clase Inner
        Inner in = new Inner();
        in.testMethod2();
    }
}
