package AnonymousInnerClasses;

/**
 * Created by Erick on 24/07/2017.
 */
public interface Radio {
    public void playRadio();
}
