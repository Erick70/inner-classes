package AnonymousInnerClasses;

/**
 * Created by Erick on 24/07/2017.
 */
public class MusicSystem {
    Speakers s = new Speakers(){
        int a = 10;
        //No constructors
        public void playSound(){
            System.out.println("Play dolby sound");
        }
    };

    Radio r = new Radio(){
        public void playRadio(){
            System.out.println("Play Radio");
        }
    };
    public void playMusic(){
        s.playSound();
        r.playRadio();
    }

    public void play(PlayAnything playAnything){
        playAnything.playStuff();
    }
    public static void main(String[] args) {
        MusicSystem ms = new MusicSystem();
        ms.playMusic();

        ms.play(new PlayAnything() {

            @Override
            public void playStuff() {
                Radio r = new Radio(){
                    public void playRadio(){
                        System.out.println("Playing station 97.1");
                    }
                };
                r.playRadio();
            }
        });
    }
}
